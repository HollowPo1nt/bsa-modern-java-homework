package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import java.util.Optional;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger poweredRequirements;

	private final PositiveInteger capacitorConsumption;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger optimalSize;

	private final PositiveInteger baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powerGridRequirements,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name == null || name.isBlank() || name.isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, powerGridRequirements, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	private AttackSubsystemImpl(String name, PositiveInteger poweredRequirements, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.poweredRequirements = poweredRequirements;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return poweredRequirements;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {

		if (capacitorConsumption.value() < poweredRequirements.value()) {
			return null;
		}

		int targetSize = target.getSize().value();
		int targetSpeed = target.getCurrentSpeed().value();

		double sizeReductionModifier = targetSize >= optimalSize.value() ? 1 : 1.0 * targetSize / optimalSize.value();
		double speedReductionModifier = targetSpeed <= optimalSpeed.value() ? 1
				: optimalSpeed.value() / (2.0 * targetSpeed);

		double damage = baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);

		return getRoundedDamage(damage);
	}

	@Override
	public String getName() {
		return name;
	}

}
