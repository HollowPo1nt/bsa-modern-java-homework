package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	String name;

	PositiveInteger powerGridConsumption;

	PositiveInteger capacitorConsumption;

	PositiveInteger impactReductionPercent;

	PositiveInteger shieldRegeneration;

	PositiveInteger hullRegeneration;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name == null || name.isBlank() || name.isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new DefenciveSubsystemImpl(name, powerGridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	private DefenciveSubsystemImpl(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name;
		this.powerGridConsumption = powerGridConsumption;
		this.capacitorConsumption = capacitorConsumption;
		this.impactReductionPercent = impactReductionPercent;
		this.shieldRegeneration = shieldRegeneration;
		this.hullRegeneration = hullRegeneration;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return capacitorConsumption;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		PositiveInteger reducedDamage = getReducedDamage(incomingDamage.damage);

		return new AttackAction(reducedDamage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	PositiveInteger getReducedDamage(PositiveInteger incomingDamage) {
		int checkedReductionPercent = impactReductionPercent.value() > 95 ? 95 : impactReductionPercent.value();

		double reducedDamage = incomingDamage.value() - (1.0 * incomingDamage.value() * checkedReductionPercent / 100);

		return getRoundedDamage(reducedDamage);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(shieldRegeneration, hullRegeneration);
	}

}
