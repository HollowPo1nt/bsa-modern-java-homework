package com.binary_studio.fleet_commander.core.subsystems.contract;

import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;

public interface Subsystem extends NamedEntity {

	PositiveInteger getPowerGridConsumption();

	PositiveInteger getCapacitorConsumption();

	default PositiveInteger getRoundedDamage(double damage) {
		return PositiveInteger.of(damage - (int) damage > 0 ? (int) damage + 1 : (int) damage);
	}

}
