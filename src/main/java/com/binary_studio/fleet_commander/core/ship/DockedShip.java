package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	String name;

	PositiveInteger shieldHP;

	PositiveInteger shieldHPMax;

	PositiveInteger hullHP;

	PositiveInteger hullHPMax;

	PositiveInteger powergridOutput;

	PositiveInteger capacitorAmount;

	PositiveInteger capacitorMaxVolume;

	PositiveInteger capacitorRechargeRate;

	PositiveInteger speed;

	PositiveInteger size;

	AttackSubsystem attackSubsystem;

	DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.shieldHPMax = shieldHP;
		this.hullHP = hullHP;
		this.hullHPMax = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorMaxVolume = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			attackSubsystem = null;

		}
		else if (subsystem.getPowerGridConsumption().value() <= powergridOutput.value()) {
			attackSubsystem = subsystem;
			powergridOutput = PositiveInteger.of(powergridOutput.value() - subsystem.getPowerGridConsumption().value());
		}
		else {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - powergridOutput.value());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			defenciveSubsystem = null;

		}
		else if (subsystem.getPowerGridConsumption().value() <= powergridOutput.value()) {
			defenciveSubsystem = subsystem;
			powergridOutput = PositiveInteger.of(powergridOutput.value() - subsystem.getPowerGridConsumption().value());
		}
		else {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - powergridOutput.value());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {

		if (attackSubsystem == null && defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();

		}
		else if (attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();

		}
		else if (defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this);
	}

}
