package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

import java.util.Optional;

public final class CombatReadyShip implements CombatReadyVessel {

	private DockedShip ship;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
	}

	@Override
	public void endTurn() {
		restoreCapacitor();
	}

	void restoreCapacitor() {
		if (ship.capacitorAmount.value() + ship.capacitorRechargeRate.value() > ship.capacitorMaxVolume.value()) {
			ship.capacitorAmount = ship.capacitorMaxVolume;

		}
		else {
			ship.capacitorAmount = PositiveInteger
					.of(ship.capacitorAmount.value() + ship.capacitorRechargeRate.value());
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return ship.name;
	}

	@Override
	public PositiveInteger getSize() {
		return ship.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return ship.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (ship.capacitorAmount.value() < ship.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}

		ship.capacitorAmount = PositiveInteger
				.of(ship.capacitorAmount.value() - ship.attackSubsystem.getCapacitorConsumption().value());

		return Optional.of(new AttackAction(ship.attackSubsystem.attack(target), this, target, ship.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		int overallDamage = ship.defenciveSubsystem.reduceDamage(attack).damage.value();
		int damageBuffer = overallDamage;

		if (ship.shieldHP.value() >= damageBuffer) {
			ship.shieldHP = PositiveInteger.of(ship.shieldHP.value() - damageBuffer);

		}
		else {
			damageBuffer -= ship.shieldHP.value();
			ship.shieldHP = PositiveInteger.of(0);

			if (ship.hullHP.value() > damageBuffer) {
				ship.hullHP = PositiveInteger.of(ship.hullHP.value() - damageBuffer);
			}
			else {
				return new AttackResult.Destroyed();
			}
		}

		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(overallDamage), this);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		if (ship.capacitorAmount.value() < ship.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}

		ship.capacitorAmount = PositiveInteger
				.of(ship.capacitorAmount.value() - ship.defenciveSubsystem.getCapacitorConsumption().value());

		RegenerateAction regenerateAction = ship.defenciveSubsystem.regenerate();

		regenerateAction = shieldAndHullRegeneration(regenerateAction);

		return Optional.of(regenerateAction);
	}

	private RegenerateAction shieldAndHullRegeneration(RegenerateAction regenerateAction) {
		int shieldRegenerated = 0;
		int hullRegenerated = 0;

		if (!ship.shieldHP.equals(ship.shieldHPMax)) {
			shieldRegenerated = ship.shieldHP.value();
			ship.shieldHP = PositiveInteger.of(ship.shieldHP.value() + regenerateAction.shieldHPRegenerated.value());

			if (ship.shieldHP.value() > ship.shieldHPMax.value()) {
				ship.shieldHP = ship.shieldHPMax;
			}

			shieldRegenerated -= ship.shieldHP.value(); // could be a negative number
		}

		if (!ship.hullHP.equals(ship.hullHPMax)) {
			hullRegenerated = ship.hullHP.value();
			ship.hullHP = PositiveInteger.of(ship.hullHP.value() + regenerateAction.hullHPRegenerated.value());

			if (ship.hullHP.value() > ship.hullHPMax.value()) {
				ship.hullHP = ship.hullHPMax;
			}

			hullRegenerated -= ship.hullHP.value(); // could be a negative number

		}

		return new RegenerateAction(PositiveInteger.of(Math.abs(shieldRegenerated)),
				PositiveInteger.of(Math.abs(hullRegenerated)));
	}

}
