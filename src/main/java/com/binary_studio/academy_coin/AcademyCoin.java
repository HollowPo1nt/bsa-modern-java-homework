package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		Integer[] arr = prices.toArray(Integer[]::new);

		int buy = 0;
		int sell = 0;
		int profit = 0;

		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i; j < arr.length; j++) {
				if (j != arr.length - 1 && arr[j] < arr[j + 1]) {
					buy = arr[i];
					sell = arr[j + 1];
				}
				else {
					profit += sell - buy;
					sell = 0;
					buy = 0;
					i = j + 1;
				}
			}
		}

		return profit;
	}

}
