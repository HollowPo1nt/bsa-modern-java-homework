package com.binary_studio.dependency_detector;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		String lib;

		for (String[] s : libraries.dependencies) {
			lib = s[1];

			for (String[] ss : libraries.dependencies) {
				if (lib.equals(ss[0])) {

					if (ss[1].equals(s[0])) {
						return false;
					}

					lib = ss[1];
				}
			}
		}

		return true;
	}

}
